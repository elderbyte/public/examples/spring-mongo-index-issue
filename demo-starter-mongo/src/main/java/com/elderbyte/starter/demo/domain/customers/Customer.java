package com.elderbyte.starter.demo.domain.customers;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Customer {

    @Id
    private ObjectId id;

    private String name;

    private MyAddress address;

}
