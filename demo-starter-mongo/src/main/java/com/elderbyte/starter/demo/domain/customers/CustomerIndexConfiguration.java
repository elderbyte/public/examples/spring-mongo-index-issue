package com.elderbyte.starter.demo.domain.customers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.stereotype.Service;

@Service
public class CustomerIndexConfiguration {

    /***************************************************************************
     *                                                                         *
     * Fields                                                                  *
     *                                                                         *
     **************************************************************************/

    private static final Logger log = LoggerFactory.getLogger(CustomerIndexConfiguration.class);

    private final MongoOperations mongoOperations;



    /***************************************************************************
     *                                                                         *
     * Constructor                                                             *
     *                                                                         *
     **************************************************************************/

    /**
     * Creates a new CloudFileIndexConfiguration
     */
    public CustomerIndexConfiguration(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    /***************************************************************************
     *                                                                         *
     * Public API                                                              *
     *                                                                         *
     **************************************************************************/

    @EventListener(ApplicationReadyEvent.class)
    public void initIndicesAfterStartup() {

        var indexOps = mongoOperations.indexOps(Customer.class);

        try {
            indexOps.dropIndex("TextIndex");
        }catch (Exception e){}

        indexOps.ensureIndex(
                TextIndexDefinition
                        .builder()
                        .named("TextIndex")
                        .onField("name")
                        .onField("address.street")
                        .build()
        );

        log.info("Created Index for Customer.");

    }

    /***************************************************************************
     *                                                                         *
     * Private methods                                                         *
     *                                                                         *
     **************************************************************************/


}
