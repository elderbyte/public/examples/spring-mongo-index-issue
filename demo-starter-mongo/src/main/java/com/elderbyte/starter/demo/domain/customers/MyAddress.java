package com.elderbyte.starter.demo.domain.customers;

public class MyAddress {

    private String street;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
