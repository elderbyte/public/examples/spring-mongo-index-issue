package com.elderbyte.starter.demo.config;

import com.elderbyte.starter.demo.domain.customers.Customer;
import com.elderbyte.starter.demo.domain.customers.MyAddress;
import org.bson.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.ArrayList;

@Configuration
@EnableMongoRepositories()
public class DefaultMongoConfiguration {


    @Bean
    public MongoCustomConversions customConversions(){
        var converters = new ArrayList<Converter<?,?>>();
        converters.add(new MyAddressToDocumentConverter());
        return new MongoCustomConversions(converters);
    }

    @WritingConverter
    public static class MyAddressToDocumentConverter implements Converter<MyAddress, Document> {
        @Override
        public Document convert(MyAddress address) {
            var doc = new Document();
            doc.put("street", address.getStreet());
            return doc;
        }
    }
}
